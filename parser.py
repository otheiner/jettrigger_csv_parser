import csv, sys

#file_csv = "Rate.csv"
#file_row_selection = "Selection.csv"
#file_columns_selection = "Columns.csv"
file_output = "output.csv"

if len(sys.argv) == 4:
	file_csv = sys.argv[1]
	file_row_selection = sys.argv[2]
	file_columns_selection = sys.argv[3]
elif len(sys.argv) == 5:
	file_csv = sys.argv[1]
	file_row_selection = sys.argv[2]
	file_columns_selection = sys.argv[3]
	file_output = sys.argv[4]
else:
	print("Wrong number of input arguments!")
	print("Provide at least 3 input .csv files. In case 4 input parameters are entered, the last parameter \nis the name of output file (otherwise it is default output.csv")
	print("Example: python3 ", sys.argv[0], " <data_file.csv> <row_selection.csv> <column_selection.csv> <output_file.csv>")
	print("\t Files have to be in the following formats: \n")
	print("<data_file.csv>\t\t\t\tItems are separated by ',' and each row is on separate line in the file. \n\t\t\t\t\tThe first row contains labels of columns. Valid .csv table can be \n\t\t\t\t\tdownloaded from https://atlas-trig-cost.cern.ch/")
	print("<row_selection.csv>\t\t\tSelection is done based on the value of cell in the first column (usually 'Name').\n\t\t\t\t\tLines should not contain comments. Every item is on the new line.")
	print("<column_selection.csv>\t\t\tEach column name is on the separate line. The first row (usually 'Name') is \n\t\t\t\t\timplicitly selected.")
	print("<output.csv>\t\t\t\tNew file with this name is created. If the file already exists, it is overridden!>")
	print("\nExample files are provided in folder example_file.")
	sys.exit()


columns_selection = []
print('\nSelecting following columns:')
with open(file_columns_selection, 'r') as columns_file:
	for line in columns_file:
		columns_selection.append(line.replace('\n', ''))		
print('\t', columns_selection)

#Load chain selection
chains = []
print('\nSelecting following rows:')
with open(file_row_selection, 'r') as selection_file:
	for line in selection_file:
		chains.append(line.split()[0])
		print('\t', line.split()[0])

#Load desired columns from csv file for required rows 
dict_csv = {}
dict_columns = {}
with open(file_csv, newline='') as data_file:
	
	reader = csv.reader(data_file, delimiter=',')
	
	#Figure out column numbers
	column_names = next(reader)
	item_counter = 0;
	for item in column_names:
		if item in columns_selection:
			dict_columns[item] = item_counter
		item_counter += 1

	#Load and parse data from csv file
	for line in reader:
		if line[0] in chains:
			values = []
			for column in dict_columns:
				values.append(line[dict_columns[column]])
			dict_csv[line[0]] = values

#Sort data as given in chains selection file and check if all chains are present
final_table = []
final_table.append(['Name'] + columns_selection)
for item in chains:
	if item in dict_csv:
		row = []
		row.append(item)
		for value in dict_csv[item]:
			row.append(value) 
		final_table.append(row)
	else:
		print('WARNING: Chain ', item, ' does not exist in provided .csv file!')

with open(file_output, 'w') as output:
    write = csv.writer(output)      
    write.writerows(final_table)		

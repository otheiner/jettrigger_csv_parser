Feel free to improve the script or add something if you think there is some feature missing. Or contact me (Ondřej Theiner, e-mail: ondrej.theiner@cern.ch).
<br />
<br />


# CSV parser

The script was created in reaction to this JIRA ticket https://its.cern.ch/jira/browse/ATR-23662 where I was supposed to choose only a few lines and columns from bigger .csv table. But since it is quite general, it can be possibly used also somewhere else.

**How to use it?** <br />
Provide at least 3 input .csv files. In case 4 input parameters are entered, the last parameter is the name of output file (otherwise it is default output.csv) <br />
Example: `python3 parser.py " <data_file.csv> <row_selection.csv> <column_selection.csv> <output_file.csv>`<br />
_Tip: You can use example files in `example_files` folder in this repo to test how the script works._

Files have to be in the following formats:
 
- `<data_file.csv>`        Items are separated by ',' and each row is on separate line in the file. The first row contains labels of columns. Valid .csv table can be  downloaded, for example, from https://atlas-trig-cost.cern.ch/ <br />
- `<row_selection.csv>`     This file contains name of rows that should be selected. Selection is done based on the value of cell in the first column (it is called 'Name' in example files). Lines should not contain comments. Every item is on the new line. <br />
- `<column_selection.csv>` This file contains name of columns that should be selected. Each column\'s name is on the separate line. The first column (usually 'Name') is implicitly selected. <br />
- `<output.csv>`           New file with this name is created. If the file already exists, it is overridden! <br />




